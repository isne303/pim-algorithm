#include <iostream>
#include "Header.h"
using namespace std;


void last_number(int *&path_shorttest, int &last_num, int row);


void main(){
						   /* A  B  C  D  E  F  G  H  I    */
	//int test_case_pim[81] = { 0, 2, 0, 0, 0, 0, 4, 0, 0,	// A 65
	//						  2, 0, 4, 2, 0, 0, 6, 0, 0,	// B 66
	//						  0, 4, 0, 3, 0, 0, 0, 0, 0,    // C 67
	//						  0, 2, 3, 0, 5, 3, 0, 0, 0,    // D 68
	//						  0, 0, 0, 5, 0, 3, 0, 0, 0,    // E 69
	//						  0, 0, 0, 3, 3, 0, 5, 4, 0,	// F 70
	//						  4, 5, 0, 0, 0, 5, 0, 0, 2,    // G 71 
	//						  0, 0, 0, 0, 0, 4, 0, 4, 0,    // H 72
	//						  0, 0, 0, 0, 0, 0, 2, 3, 0  }; // I 73
	int row, col;
	int **matrix;
	cout << "How many nodes: ";
	cin >> row;   

	if (row != 0)
	{
		col = row;
		// dynamically allocate an array
		matrix = new int *[row];
		cout << endl;
		for (int count = 0; count < row; count++)
		{
			matrix[count] = new int[col];
		}
		matrixs_default(matrix, row, col);	//set default.
		matrixs(matrix, row, col);
		
		// input element for matrix
		cout << "----------------------------------\n\n#### Adjaccency matrix <array>\n\n";
		//   A B C D ....
		cout << "\t"; for (int i = 65; i < 65 + row; i++){ char tmp = i;	cout << tmp << " "; }
		char tmp = 65;
		for (int i = 0; i < row; i++)
		{
			cout << endl << "      " << tmp;
			for (int j = 0; j < col; j++)
			{
				cout << " " << matrix[i][j];
			}
			tmp++;
		}
		//close upper func for w8 maintain !
		cout << "\n\n----------------------------------\n\n#### Adjaccency linked list\n\n";

		//Testing node class.
		Daigraph ex;			/*Creat Daigraph*/
		Node B;
		Edge C;
		int j = 65;
		int node_round = 0;		//count round!!
		for (int i = 65; i < 65 + row; i++){
			ex.node.push_back(B);	/* Done it to creat Object 'B' in list of 'ex' */
		}

		creat_graph(ex, B, C, j, node_round, row, col, matrix);
		print_node(ex, row);		// Func for print graph!
		///////    CHECKING  GRAPH   TYPE.
		/*
		cout << "\n\n----------------------------------\n\n#### Check the graph to the answer\n\n";
		multigraph(ex, row);
		pseudograph(ex);
		digraph(ex, row);
		weightgraph(ex, row);
		completegraph(ex, row);
		*/
		cout << "\n\n----------------------------------\n\n#### Pim Graph ?\n\n";
		// Next find shorttest path.
		int *path_tmp = new int[row];
		int *path_shorttest = new int[row];
		//int node_int;
		int last_num = 0;		//collect last cost in shorttest path.
		char node_char;
		
		//add dafault to array set ['0']
			for (int i = 0; i < row; i++)
				{
					path_tmp[i] = 0;
					path_shorttest[i] = 0;
				}
		//PIM PROGRAME
			cout << "Which is start Node ? : ";	cin >> node_char;
			cout << endl;
			path_tmp[node_char - 65] = -1;
			for (int j = 0; j < row-1 ; j++){
				pime(ex, node_char, path_tmp, path_shorttest, row, matrix);
			}/*
			for (int i = 0; i < row; i++)
			{
				cout << path_tmp[i] << " ";
			}
			cout << endl;
			for (int i = 0; i < row; i++)
			{
				cout << path_shorttest[i] << " ";
			}*/
			cout << endl;
	
	}
	else
	{
	cout << "\n\n>>>>>> END PROCESS can't creat graph with num node = '0' !\n\n";
	}

	system("PAUSE");
}
	/////////// END of Main //////////////

void last_number(int *&path_shorttest, int &last_num , int row)
{
	for (int o = 0; 0 < row; o++)
	{
		if (path_shorttest[o] == 0)
		{
			last_num = path_shorttest[o - 1];	//return last_num;
			break;
		}
	}
}
