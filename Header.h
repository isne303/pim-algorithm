#include <queue>
#include <stack>
#include <list>
using namespace std;

class Node;
class Edge;

class Daigraph {
public:
	list<Node> node;		//for collect node name.
};

class Node{
public:
	//Valiable of class Node.
	list<Edge> edge;
	char Node_name = 0 ;
	list<Edge>::iterator ToEdge;	//NEW LINK BACK TO EDGE BOX.
};

class Edge{

public:
	char sorce_node = 0;	 //NODE NAME OF SORCE NODE.
	char destination;	 //point to desination node!
	int cost = 0;		 //distance.
	list<Node>::iterator ToNode;	//LINK TO NODE (DESTINATION).
};


// ALL FUNCTION HERE!

void creat_graph(Daigraph &ex, Node B, Edge C, int j, int node_round, int row, int col, int** matrix)
{
	for (list<Node>::iterator node = ex.node.begin(); node != ex.node.end(); ++node)	//it => ex.node.begin()
	{
		int tmps = 64;
		int edge_round = 0;		//cout edge!!
		node->Node_name = j;
		char ask = 65;
		char identify = 65;
		//
		int round = 1;
		for (int i = 0; i < row; i++)	{ node->edge.push_back(C); }			// Create Edge in list<Edge> edge.			

		////////////////////////////////// Edge part //////////////////////////////////////////
		for (list<Edge>::iterator edge = node->edge.begin(); edge != node->edge.end(); ++edge)
		{
			edge->cost = matrix[node_round][edge_round];
			tmps++;		//tmps ' 65 '
			//		identify ' 65 ' or ' A '
			for (list<Node>::iterator node2 = ex.node.begin(); node2 != ex.node.end(); ++node2)	//asking??
			{
				if (identify == ask){
					//cout << "ID : " << identify << endl;
					if (edge->cost >= 0){			//THIS SEGMENT IMPLEMENT POINTER FOR EACH OTHER.
						edge->ToNode = node2; //POINT TO NODE ??
						node2->ToEdge = edge; //POINT BACK TO EDGE !!!
						edge->sorce_node = node->Node_name; //IDENTIFY SORCE NODE!!!
						//cout << edge->sorce_node << endl;

					}
					ask++;
					break;
				}
				identify++;
			}
			identify = 65;	//reset identify
			edge->destination = tmps;
			edge_round++;
			//cout << "BOX EDGE" <<endl;
		}
		j++;
		node_round++;
	}
}

void print_node(Daigraph ex, int row){

	for (list<Node>::iterator node = ex.node.begin(); node != ex.node.end(); ++node){

		for (list<Edge>::iterator edge = node->edge.begin(); edge != node->edge.end(); ++edge){

			if (edge == node->edge.begin()){
				cout << node->Node_name << " ";
			}
			for (int i = 0; i < row; i++)
			{
				if (edge == node->edge.end())
					cout << edge->cost << "-> NULL";					//print node name push it in list.
			}
			if (edge != node->edge.end() && edge->cost > 0){
				cout << "[" << edge->cost << "-> " << edge->ToNode->Node_name << "] -- ";
			}
		}cout << endl;
	}
}

int matrixs(int** &x, int row, int col){

	char sorce = 65;				// Sorce.
	char destination = 65;			// Destination.
	int index_testcase = 0;
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			cout << "Input cost of Sorce/Destination " << sorce << "-->" << destination << " : ";	//fix this func for easy looking.
			cin >> x[i][j];										// is there any equivalent declaration here?
			if (j + 1 == col){ cout << endl; }
			destination++;
			index_testcase++;
		}
		/*reset sedtination*/
		destination = 65;
		sorce++;
	}
	return **x;
}

int matrixs_default(int** &x, int row, int col){

	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			x[i][j] = 0;										// is there any equivalent declaration here?
		}
	}
	
	return **x;
}





//PIM !!  <3
void pime(Daigraph ex, char &node_char, int *&path_tmp, int *&path_shorttest, int row, int **matrix)
{
	//cout << "TEST Message ! \n";

	char des_node;
	list<Node>::iterator current_node;
	for (list<Node>::iterator node = ex.node.begin(); node != ex.node.end(); ++node)
	{
		if (node->Node_name == node_char)
		{
			current_node = node;		//We found it node 'A'.
			//cout << "Hello Node '"<<node->Node_name <<"' \n\n"; 
			
			break;
		}
	}
	
	int index_count = 0;		//index counter?
	for (list<Edge>::iterator edge = current_node->edge.begin(); edge != current_node->edge.end(); ++edge)
	{
		
		if (edge->cost != 0)
		{
			if (path_tmp[index_count] != -1)	//if it already visited skip this loop.
			{
				if (path_tmp[index_count] == 0)	//Nerver been use bofore So add it !
				{
					path_tmp[index_count] = edge->cost;
					//cout << "\n :" << edge->cost;
				}
				else if( path_tmp[index_count] > 0 && path_tmp[index_count] > edge->cost) //If it already have old cost then check it.
				{													 //If it less than old cost update it.
					path_tmp[index_count] = edge->cost;		//Update it :) 
					//cout << "\n :" << edge->cost;
				}
			}
			
		}
		//else only move next to next index
		index_count++;
	}
	
//CHOOSE THE BEST PART. --->

	int smallest = 0;
//set smallnum
	for (int i = 0; i < row; i++)
	{
		if (path_tmp[i] != 0 && path_tmp[i] != -1 )
		{
			smallest = path_tmp[i];		//Already set.
			//cout << " Smallest : " << smallest;
			break;
		}
	}
	
	for (int i = 0; i < row; i++)
	{
		if (path_tmp[i] != 0 && path_tmp[i] != -1)
		{
			if (path_tmp[i] < smallest && path_tmp[i] != -1 )
			{
				smallest = path_tmp[i];
			}
		}
	}
	int collect;
	for (int i = 0; i < row; i++)
	{
		if (path_tmp[i] == smallest)
		{
			collect = i;
			node_char = i+65;			//collect area of node destination;
			break;
		}
	}


	//tmp_stack use here.
	//current_node;
	bool checkBreak = false;
	char tmp_node;
	for (list<Edge>::iterator edge2 = current_node->edge.begin(); edge2 != current_node->edge.end(); ++edge2)
	{
		if (edge2->ToNode->Node_name == node_char && edge2->cost != 0){
			cout << "\n"<< edge2->sorce_node << " <--> ";
			
				break;}
		if (edge2->ToNode->Node_name == node_char && edge2->cost == 0){
			for (int check = 0; check < row; check++)
			{
				if (edge2->cost != smallest)
				{
					for (list<Node>::iterator node = ex.node.begin(); node != ex.node.end(); node++)	//SEARCH NODE?
					{
						for (list<Edge>::iterator edge = node->edge.begin(); edge != node->edge.end(); edge++)
						{
							if (edge->ToNode->Node_name == node_char /*'F'*/)
							{
								if (edge->cost == smallest)/*3*/
								{
									/*cout << edge->cost << " --> We found it";
									cout << edge->sorce_node;*/
									tmp_node = edge->sorce_node;
									checkBreak = true;
									break;
								}
							}

						}
						if (checkBreak == true){ break; }
					}
					//cout << edge2->cost;
					//cout << " This is collect : " << collect;
					break;
					//set new current node;
				}
			}
			cout << "\n"<< tmp_node << " <--> ";
			
			break;
		}
	}
	
	cout << node_char ;
	for (int i = 0; i < row; i++)
	{
	
		if (path_tmp[i] == smallest)
		{
			path_tmp[i] = -1;				//move data from path_tmp and set it to bre RIP.
			break;
		}
	}
	//We get smallest cost now .Then add it to shorttest path.
	for (int i = 0; i < row; i++)
	{
		if (path_shorttest[i] == 0)
		{
			path_shorttest[i] = smallest;
			break;
		}
		/*else go next index*/
	}
	
}